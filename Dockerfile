FROM registry.lendfoundry.com/base:beta8

ADD ./src/CapitalAlliance.ApplicationProcessor.Abstractions /app/CapitalAlliance.ApplicationProcessor.Abstractions
WORKDIR /app/CapitalAlliance.ApplicationProcessor.Abstractions
RUN dnu restore --ignore-failed-sources --no-cache -s http://build.lendfoundry.co/guestAuth/app/nuget/v1/FeedService.svc/  -s https://www.nuget.org/api/v2/ -s https://www.myget.org/F/aspnetvnext/api/v2/
RUN dnu build

ADD ./src/CapitalAlliance.ApplicationProcessor.Persistence /app/CapitalAlliance.ApplicationProcessor.Persistence
WORKDIR /app/CapitalAlliance.ApplicationProcessor.Persistence
RUN dnu restore --ignore-failed-sources --no-cache -s http://build.lendfoundry.co/guestAuth/app/nuget/v1/FeedService.svc/  -s https://www.nuget.org/api/v2/ -s https://www.myget.org/F/aspnetvnext/api/v2/
RUN dnu build

ADD ./src/CapitalAlliance.ApplicationProcessor /app/CapitalAlliance.ApplicationProcessor
WORKDIR /app/CapitalAlliance.ApplicationProcessor
RUN dnu restore --ignore-failed-sources --no-cache -s http://build.lendfoundry.co/guestAuth/app/nuget/v1/FeedService.svc/  -s https://www.nuget.org/api/v2/ -s https://www.myget.org/F/aspnetvnext/api/v2/
RUN dnu build

ADD ./src/CapitalAlliance.ApplicationProcessor.Api /app/CapitalAlliance.ApplicationProcessor.Api
WORKDIR /app/CapitalAlliance.ApplicationProcessor.Api
RUN dnu restore --ignore-failed-sources --no-cache -s http://build.lendfoundry.co/guestAuth/app/nuget/v1/FeedService.svc/  -s https://www.nuget.org/api/v2/ -s https://www.myget.org/F/aspnetvnext/api/v2/
RUN dnu build

EXPOSE 5000
ENTRYPOINT dnx kestrel
﻿using System;
using System.Threading.Tasks;
using LendFoundry.Foundation.Services;
using Microsoft.AspNet.Mvc;

namespace CapitalAlliance.ApplicationProcessor.Api.Controllers
{
    [Route("/application")]
    public class ApplicationController : ExtendedController
    {
        public ApplicationController(IApplicationProcessorClientService applicationService)
        {
            if (applicationService == null)
                throw new ArgumentNullException(nameof(applicationService));

            ApplicationService = applicationService;
        }

        private IApplicationProcessorClientService ApplicationService { get; }

        [HttpPost("submit/business")]
        public async Task<IActionResult> SubmitBusinessApplication([FromBody]SubmitBusinessApplicationRequest submitBusinessApplicationRequest)
        {
            return await ExecuteAsync(async () => Ok(await ApplicationService.SubmitBusinessApplication(submitBusinessApplicationRequest)));
        }   
    }
}

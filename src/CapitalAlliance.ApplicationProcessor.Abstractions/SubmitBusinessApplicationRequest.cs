﻿using LendFoundry.Business.Applicant;
using System;

namespace CapitalAlliance.ApplicationProcessor
{
    public class SubmitBusinessApplicationRequest : ISubmitBusinessApplicationRequest
    {
        public string Email { get; set; }
        public double RequestedAmount { get; set; }
        public string PurposeOfLoan { get; set; }
        public DateTimeOffset DateNeeded { get; set; }
        public string LegalBusinessName { get; set; }
        public string AddressLine1 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public int AddressType { get; set; }
        public string BusinessTaxID { get; set; }
        public string SICCode { get; set; }
        public DateTimeOffset BusinessStartDate { get; set; }
        public BusinessType BusinessType { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string SSN { get; set; }
        public string DateOfBirth { get; set; }
        public string Ownership { get; set; }
        public string HomeAddressLine1 { get; set; }
        public string HomeCity { get; set; }
        public string HomeState { get; set; }
        public string HomePostalCode { get; set; }
        public string Mobile { get; set; }
        public double AnnualRevenue { get; set; }
        public double AverageBankBalances { get; set; }
        public bool HaveExistingLoan { get; set; }
    }
}

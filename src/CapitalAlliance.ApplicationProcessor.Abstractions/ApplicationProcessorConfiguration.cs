﻿using LendFoundry.Security.Identity;

namespace CapitalAlliance.ApplicationProcessor
{
    public class ApplicationProcessorConfiguration
    {
        public CaseInsensitiveDictionary<string> Statuses { get; set; }
        public CaseInsensitiveDictionary<RuleDetail> Rules { get; set; }
        public string[] ReApplyStatuseCodes { get; set; }
        public int ReApplyTimeFrameDays { get; set; }
        public string PrimaryCreditBureau { get; set; }
        public string SecondaryCreditBureau { get; set; }
    }
}

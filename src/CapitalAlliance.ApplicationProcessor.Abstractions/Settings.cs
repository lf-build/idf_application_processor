﻿using LendFoundry.Foundation.Services.Settings;

namespace CapitalAlliance.ApplicationProcessor
{
    public static class Settings
    {
        public static string ServiceName { get; } = "application-processor";

        private static string Prefix { get; } = ServiceName.ToUpper();

        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION_HOST", "configuration", $"{Prefix}_CONFIGURATION_PORT");

        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB", "eventhub");

        public static ServiceSettings Tenant { get; } = new ServiceSettings($"{Prefix}_TENANT", "tenant");

        public static DatabaseSettings Mongo { get; } = new DatabaseSettings($"{Prefix}_MONGO_CONNECTION", "mongodb://mongo", $"{Prefix}_MONGO_DATABASE", "application-processor");

      
        public static ServiceSettings LookUpService { get; } = new ServiceSettings($"{Prefix}_LOOKUP_HOST", "lookup-service", $"{Prefix}_LOOKUP_PORT");
        public static ServiceSettings StatusManagement { get; } = new ServiceSettings($"{Prefix}_STATUSMANAGEMENT_HOST", "status-management", $"{Prefix}_STATUSMANAGEMENT_PORT");      
        //AddApplicationService
        public static ServiceSettings BusinessApplication { get; } = new ServiceSettings($"{Prefix}_BUSINESSAPPLICATION_HOST", "businessapplication", $"{Prefix}_BUSINESSAPPLICATION_PORT");
        //AddApplicantService
        public static ServiceSettings BusinessApplicant { get; } = new ServiceSettings($"{Prefix}_BUSINESSAPPLICANT_HOST", "businessapplicant", $"{Prefix}_BUSINESSAPPLICANT_PORT");
        //DataMerch
        public static ServiceSettings DataMerchSyndication { get; } = new ServiceSettings($"{Prefix}_DATAMERCH_HOST", "datamerch", $"{Prefix}_DATAMERCH_PORT");
        //WhitePages
        public static ServiceSettings WhitePagesSyndication { get; } = new ServiceSettings($"{Prefix}_WHITEPAGES_HOST", "whitepages", $"{Prefix}_WHITEPAGES_PORT");
        //Yelp
        public static ServiceSettings YelpSyndication { get; } = new ServiceSettings($"{Prefix}_YELP_HOST", "yelp", $"{Prefix}_YELP_PORT");
        //BBB Search
        public static ServiceSettings BBBSearchSyndication { get; } = new ServiceSettings($"{Prefix}_BBBSEARCH_HOST", "bbbsearch", $"{Prefix}_BBBSEARCH_PORT");
    }
}

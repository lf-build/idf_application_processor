﻿using System.Threading.Tasks;

namespace CapitalAlliance.ApplicationProcessor
{
    public interface IApplicationProcessorClientService
    {       
        Task<LendFoundry.Business.Application.IApplicationResponse> SubmitBusinessApplication(ISubmitBusinessApplicationRequest submitBusinessApplicationRequest);

    }
}

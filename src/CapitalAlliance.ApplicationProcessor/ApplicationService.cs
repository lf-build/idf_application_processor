﻿using CreditExchange.StatusManagement;
using CreditExchange.Syndication.BBBSearch;
using CreditExchange.Syndication.Datamerch;
//using CreditExchange.Syndication.Whitepages;
//using CreditExchange.Syndication.Yelp;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Lookup;
using Microsoft.CSharp.RuntimeBinder;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
//using CreditExchange.Email;


namespace CapitalAlliance.ApplicationProcessor
{
    public class ApplicationService : IApplicationProcessorClientService
    {
        public ApplicationService(ApplicationProcessorConfiguration applicationProcessorConfiguration, IEntityStatusService entityStatusService,
           ILookupService lookupService,
            LendFoundry.Business.Application.IApplicationService businessApplicationService,
            IDataMerchService dataMerchService,
            //IYelpService yelpService,
            // IWhitepagesService whitepagesService,
            // IEmailService emailService,
            IBBBSearchService bbbSearchService
            )
        {
            if (businessApplicationService == null)
                throw new ArgumentNullException(nameof(businessApplicationService));

            if (entityStatusService == null)
                throw new ArgumentNullException(nameof(entityStatusService));

            if (dataMerchService == null)
                throw new ArgumentNullException(nameof(dataMerchService));

            //if (yelpService == null)
            //    throw new ArgumentNullException(nameof(yelpService));

            if (bbbSearchService == null)
                throw new ArgumentNullException(nameof(bbbSearchService));

            //if (whitepagesService == null)
            //    throw new ArgumentNullException(nameof(whitepagesService));

            //if (emailService == null)
            //    throw new ArgumentNullException(nameof(emailService));

            LookupService = lookupService;

            BusinessApplicationService = businessApplicationService;
            EntityStatusService = entityStatusService;
            ApplicationProcessorConfiguration = applicationProcessorConfiguration;
            DataMerchService = dataMerchService;
            //YelpService = yelpService;
            BBBSearchService = bbbSearchService;
           // WhitepagesService = whitepagesService;
            //EmailService = emailService;
        }

        private ILookupService LookupService { get; }
        private ApplicationProcessorConfiguration ApplicationProcessorConfiguration { get; }

        private IEntityStatusService EntityStatusService { get; }

        private LendFoundry.Business.Application.IApplicationService BusinessApplicationService { get; }

        private IDataMerchService DataMerchService { get; }
        //private IYelpService YelpService { get; }
        private IBBBSearchService BBBSearchService { get; }

        //private IWhitepagesService WhitepagesService { get; }

        private ITenantTime TenantTime { get; }

        //private IEmailService EmailService { get; }


        private static string GetResultValue(dynamic data)
        {
            Func<dynamic> resultProperty = () => data.result;

            return HasProperty(resultProperty) ? GetValue(resultProperty) : string.Empty;
        }

        private static bool HasProperty<T>(Func<T> property)
        {
            try
            {
                property();
                return true;
            }
            catch (RuntimeBinderException)
            {
                return false;
            }
        }

        private static T GetValue<T>(Func<T> property)
        {
            return property();
        }
        //private async Task<bool> CheckIfDuplicateApplicationExists(UniqueParameterTypes parameterName, string parameterValue)
        //{
        //    var result = false;
        //    //var application = await ApplicationFilterService.DuplicateExistsData(parameterName, parameterValue);

        //    //if (application != null &&
        //    //   !(ApplicationProcessorConfiguration.ReApplyStatuseCodes.Contains(application.StatusCode) &&
        //    //   DateTimeOffset.Now.Subtract(application.Submitted).Days >= ApplicationProcessorConfiguration.ReApplyTimeFrameDays))
        //    //{
        //    //    result = true;
        //    //}
        //    return result;
        //}

        #region Business Application

        private void EnsureBusinessInputIsValid(ISubmitBusinessApplicationRequest applicationRequest)
        {
            if (applicationRequest == null)
                throw new ArgumentNullException(nameof(applicationRequest), "application cannot be empty");

            if (string.IsNullOrEmpty(applicationRequest.Mobile))
                throw new ArgumentException("Personal mobile Number is mandatory");

            if (string.IsNullOrEmpty(applicationRequest.Email))
                throw new ArgumentException("Personal email address is mandatory");

            if (string.IsNullOrEmpty(applicationRequest.SSN))
                throw new ArgumentException("SSN is mandatory");

            if (applicationRequest.RequestedAmount <= 0)
                throw new ArgumentNullException("Requested amount is mandatory");

            if (string.IsNullOrWhiteSpace(applicationRequest.PurposeOfLoan))
                throw new ArgumentNullException("Purpose Of Loan  is mandatory");

            //var purposeOfLoanExists = LookupService.GetLookupEntry("application-purposeOfLoan", applicationRequest.PurposeOfLoan);
            //if (purposeOfLoanExists == null)
            //    throw new InvalidArgumentException("Purpose of loan is not valid");

            if (string.IsNullOrEmpty(applicationRequest.FirstName))
                throw new ArgumentNullException($"{nameof(applicationRequest.FirstName)} is mandatory");

            if (string.IsNullOrEmpty(applicationRequest.LastName))
                throw new ArgumentNullException($"{nameof(applicationRequest.LastName)} is mandatory");

            if (applicationRequest.DateOfBirth == null)
                throw new ArgumentNullException($"{nameof(applicationRequest.DateOfBirth)} is mandatory");

            if (string.IsNullOrWhiteSpace(applicationRequest.BusinessTaxID))
                throw new ArgumentNullException($"{nameof(applicationRequest.BusinessTaxID)} is mandatory");

            if (string.IsNullOrWhiteSpace(applicationRequest.LegalBusinessName))
                throw new ArgumentNullException($"{nameof(applicationRequest.LegalBusinessName)} is mandatory");

            if (applicationRequest.AnnualRevenue <= 0)
                throw new ArgumentNullException($"{nameof(applicationRequest.AnnualRevenue)} is mandatory");

            if (applicationRequest.AverageBankBalances <= 0)
                throw new ArgumentNullException($"{nameof(applicationRequest.LegalBusinessName)} is mandatory");

            if (string.IsNullOrWhiteSpace(applicationRequest.AddressLine1))
                throw new ArgumentException("Current address line1 is mandatory");

            if (string.IsNullOrWhiteSpace(applicationRequest.City))
                throw new ArgumentNullException("Current address city is mandatory");

            if (string.IsNullOrWhiteSpace(applicationRequest.State))
                throw new ArgumentNullException("Current address state is mandatory");

            if (string.IsNullOrWhiteSpace(applicationRequest.PostalCode))
                throw new ArgumentNullException("Current address pincode is mandatory");
        }

        private LendFoundry.Business.Applicant.IApplicantRequest GetBusinessApplicantRequest(ISubmitBusinessApplicationRequest request)
        {
            LendFoundry.Business.Applicant.IApplicantRequest applicantRequest = new LendFoundry.Business.Applicant.ApplicantRequest();

            applicantRequest.UserName = request.Email;
            applicantRequest.Password = "sigma123";
            applicantRequest.LegalBusinessName = request.LegalBusinessName;
            applicantRequest.BusinessTaxID = request.BusinessTaxID;
            applicantRequest.BusinessType = request.BusinessType;
            applicantRequest.SICCode = request.SICCode;
            applicantRequest.BusinessStartDate = request.BusinessStartDate;
            applicantRequest.PrimaryEmail = new LendFoundry.Business.Applicant.EmailAddress
            {
                Email = request.Email,
                EmailType = LendFoundry.Business.Applicant.EmailType.Work,
            };
            applicantRequest.Owners = new List<LendFoundry.Business.Applicant.IOwner>
            {
                new LendFoundry.Business.Applicant.Owner
                {
                    FirstName = request.FirstName,
                    LastName = request.LastName,
                    DOB = request.DateOfBirth,
                    SSN = request.SSN,
                    Ownership = request.Ownership
                }
            };
            applicantRequest.EmailAddresses = new List<LendFoundry.Business.Applicant.IEmailAddress>
            {
                new LendFoundry.Business.Applicant.EmailAddress
                {
                    Email = request.Email,
                    EmailType = LendFoundry.Business.Applicant.EmailType.Work,
                    IsDefault = true
                }
            };
            applicantRequest.PhoneNumbers = new List<LendFoundry.Business.Applicant.IPhoneNumber>
            {
                new LendFoundry.Business.Applicant.PhoneNumber
                {
                    IsDefault = true,
                    Phone = request.Mobile,
                    PhoneType = LendFoundry.Business.Applicant.PhoneType.Mobile
                }
            };
            applicantRequest.Addresses = new List<LendFoundry.Business.Applicant.IAddress>
                    {
                        new LendFoundry.Business.Applicant.Address {
                            AddressLine1 = request.AddressLine1,
                            AddressType = LendFoundry.Business.Applicant.AddressType.Business,
                            City = request.City,
                            PinCode = request.PostalCode,
                            State = request.State,
                            IsDefault = true
                        },
                         new LendFoundry.Business.Applicant.Address {
                           AddressLine1 = request.HomeAddressLine1,
                            AddressType = LendFoundry.Business.Applicant.AddressType.Home,
                            City = request.HomeCity,
                            PinCode = request.HomePostalCode,
                            State = request.HomeState,
                            IsDefault = true
                        }
                    };

            return applicantRequest;
        }

        private LendFoundry.Business.Application.IApplicationRequest GetBusinessApplicationRequest(ISubmitBusinessApplicationRequest request)
        {
            LendFoundry.Business.Application.IApplicationRequest applicationRequest = new LendFoundry.Business.Application.ApplicationRequest
            {
                PrimaryApplicant = GetBusinessApplicantRequest(request),
                PurposeOfLoan = (LendFoundry.Business.Application.PurposeOfLoan)Enum.Parse(typeof(LendFoundry.Business.Application.PurposeOfLoan), request.PurposeOfLoan),
                RequestedAmount = request.RequestedAmount,
                DateNeeded = request.DateNeeded
            };
            return applicationRequest;
        }

        public async Task<LendFoundry.Business.Application.IApplicationResponse> SubmitBusinessApplication(ISubmitBusinessApplicationRequest submitBusinessApplicationRequest)
        {
            EnsureBusinessInputIsValid(submitBusinessApplicationRequest);

            var isExists = false;

            //isExists = await CheckIfDuplicateApplicationExists(UniqueParameterTypes.Mobile, submitBusinessApplicationRequest.Mobile);
            //if (isExists)
            //    throw new InvalidOperationException("Application with same mobile number already exists");

            //isExists = await CheckIfDuplicateApplicationExists(UniqueParameterTypes.Pan, submitBusinessApplicationRequest.SSN);
            //if (isExists)
            //    throw new InvalidOperationException("Application with same SSN already exists");

            //isExists = await CheckIfDuplicateApplicationExists(UniqueParameterTypes.Email, submitBusinessApplicationRequest.Email);
            //if (isExists)
            //    throw new InvalidOperationException("Application with same email id already exists");

            var applicationRequest = GetBusinessApplicationRequest(submitBusinessApplicationRequest);

            // Application Submitted 
            var application = await BusinessApplicationService.Add(applicationRequest);

            //EmailData objEmailData = new EmailData();
            
            //await EmailService.SendEmail("application", application.ApplicationNumber, objEmailData);

            //await EmailService.Send("application", application.ApplicationNumber, "", "", null, null);

            var test1 = await DataMerchService.SearchMerchant("application", application.ApplicationNumber, submitBusinessApplicationRequest.BusinessTaxID);

            //var test2 = await YelpService.GetPhoneDetails("application", application.ApplicationNumber, "+" + submitBusinessApplicationRequest.Mobile);

            //CreditExchange.Syndication.Whitepages.Request.LeadVerifyRequest objLeadVerifyRequest = new CreditExchange.Syndication.Whitepages.Request.LeadVerifyRequest();
            //objLeadVerifyRequest.City = submitBusinessApplicationRequest.City;
            //objLeadVerifyRequest.Email = submitBusinessApplicationRequest.Email;
            //objLeadVerifyRequest.FirstName = submitBusinessApplicationRequest.FirstName;
            //objLeadVerifyRequest.LastName = submitBusinessApplicationRequest.LastName;
            //objLeadVerifyRequest.Phone = submitBusinessApplicationRequest.Mobile;
            //objLeadVerifyRequest.PostalCode = submitBusinessApplicationRequest.PostalCode;
            //objLeadVerifyRequest.StateCode = submitBusinessApplicationRequest.State;
            //objLeadVerifyRequest.StreetLine1 = submitBusinessApplicationRequest.AddressLine1;
            //objLeadVerifyRequest.CountryCode = "01";
            //objLeadVerifyRequest.IpAddress = "192.168.1.67";
            //objLeadVerifyRequest.Name = submitBusinessApplicationRequest.FirstName + " " + submitBusinessApplicationRequest.LastName;
            //objLeadVerifyRequest.StreetLine2 = "Suite 100";
            //var test3 = await WhitepagesService.VerifyLead("application", application.ApplicationNumber, objLeadVerifyRequest);

            //CreditExchange.Syndication.BBBSearch.Request.BBBSearchRequest objBBBSearchRequest = new CreditExchange.Syndication.BBBSearch.Request.BBBSearchRequest();
            //objBBBSearchRequest.PageSize = 10;
            //objBBBSearchRequest.PageNumber = 2;
            //objBBBSearchRequest.OrganizationName = submitBusinessApplicationRequest.LegalBusinessName;
            //objBBBSearchRequest.IsBBBAccredited = false;
            //objBBBSearchRequest.IsReportable = false;
            //var test3 = await BBBSearchService.SearchOrganization("application", application.ApplicationNumber, objBBBSearchRequest);

            // change status to application submitted 
            EntityStatusService.ChangeStatus("application", application.ApplicationNumber, ApplicationProcessorConfiguration.Statuses["ApplicationSubmitted"]);

            //await DataAttributesEngine.SetAttribute("application", application.ApplicationNumber, "extension", GetApplicationExtension(submitApplicationRequest));
            return application;
        }

        #endregion
    }
}

﻿using System;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;

namespace CapitalAlliance.ApplicationProcessor.Client
{
    public class ApplicationProcessorClientServiceFactory : IApplicationProcessorClientServiceFactory
    {
        public ApplicationProcessorClientServiceFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
        }

        private IServiceProvider Provider { get; }

        private string Endpoint { get; }

        private int Port { get; }

        public IApplicationProcessorClientService Create(ITokenReader reader)
        {
            var client = Provider.GetServiceClient(reader, Endpoint, Port);
            return new ApplicationProcessorClientService(client);
        }
    }
}
